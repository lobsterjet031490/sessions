// [SECTION] While Loop
/*let count = 5;

while (count !== 0) {
	console.log("Current value of count: " + count);
	count--;
}*/


// [SECTION] Do-while loop
/*let number = Number(prompt("Give me a number:"));

do {
	console.log("Current value of number: " + number);

	number += 1;
} while (number <= 10)*/


// [SECTION] For Loop
/*for(let count = 0; count <=20; count++){
	console.log("Current for loop value: " + count);
}*/


// let my_string = "earl";


// To get the length of a string
// console.log(my_string.length);


// To get a specific letter in a string
// console.log(my_string[2]);

// Loops through each letter in the string and will keep iterating as long as the current index is less than the length of the string.
// for(let index = 0; index < my_string.length; index++){
//	console.log(my_string[index]); //starts at letter 'e' and will keep printing each letter up to 'L'.
// }

let my_name = "Jeter Gastardo"
/*for(let index = 0; index < my_name.length; index++){
	if(my_name[index] !== 'a' && my_name[index] !== 'e' && my_name[index] !== 'o' && my_name[index] !== 'i' && my_name[index] !== 'u')
		console.log(my_name[index]);
}*/

// or

for(let index = 0; index < my_name.length; index++){
	// For filtering the vowels
	if(
		my_name.index.toLowerCase() == "a" ||
		my_name.index.toLowerCase() == "e" ||
		my_name.index.toLowerCase() == "i" ||
		my_name.index.toLowerCase() == "o" ||
		my_name.index.toLowerCase() == "u" 
		){

		// If the current letter matches any of the vowels, it will just print an empty string
		continue;
	} else {
		// Prints the current letter if it is not a vowel
		console.log(my_name[index]);
	}
}

/*let name_two = "rafael";

for(let index = 0; index < name_two.length; index++){
	if(name_two[index].toLowerCase() == "a"){
		continue;
	}if(name_two[index].toLowerCase() == "e"){
		break;
	}
}*/


