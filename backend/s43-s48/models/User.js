const mongoose = require('mongoose');

// Schema

   
    // The "enrollees" property/field will be an array of objects containing the user IDs and the date and time that the user enrolled to the course
    // We will be applying the concept of referencing data to establish a relationship between our courses and users 
    

const user_schema = new mongoose.Schema({
    firstName: {
        type: String,
        required: [true, "Firstname is required"]
    },
    lastName: {
        type: String,
        required: [true, "Lastname is required"]
    },
    email: {
        type: String,
        required: [true, "Email is required"]
    },
   password: {
        type: String,
        required: [true, "Password is required"]
    },
    isAdmin: {
        type: Boolean,
        default: true
    },
   mobileNo: {
        type: String,
        required: [true, "Mobile Number is required"]
    },
    enrollments: [
        {
            courseId: {
                type: String,
                required: [true, "Course ID is required"]
            },
            enrolledOn: {
                type: Date,
                default: new Date() 
            },
           status: {
                type: String,
                default: "Enrolled" 
            }
        }
    ]
})

module.exports = mongoose.model("User", user_schema);

