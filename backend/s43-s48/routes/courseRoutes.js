const express = require('express');
const router = express.Router();
const auth = require('../auth.js');
const CourseController = require('../controllers/CourseController.js');

// You can destructure the 'auth' variable to extract the function being exported from it. You can then use the functions directly without having to use dot (.) notation.
// const {verify, verifyAdmin} = auth;

router.post('/', auth.verify, auth.verifyAdmin, (request, response) => {
	CourseController.addCourse(request, response)
});

// Get all courses
router.get('/all', (request, response) => {
	CourseController.getAllCourses(request, response);
});

router.get('/', (request, response) => {
	CourseController.getAllActiveCourses(request, response);
});

router.get('/:id', (request,response) => {
	CourseController.getCourse(request, response);
})

router.put('/:id', auth.verify, auth.verifyAdmin, (request, response) => {
	CourseController.updateCourse(request, response);
})

router.put('/:id/archive', auth.verify, (request, response) => {
	CourseController.archiveCourse(request, response);
})

router.put('/:id/activate', auth.verify, auth.verifyAdmin, (request, response) => {
	CourseController.activateCourse(request, response);
})

router.post('/search', (request, response) => {
	CourseController.searchCourses(request, response);
});

module.exports = router;