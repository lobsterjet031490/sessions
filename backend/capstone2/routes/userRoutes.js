const express = require('express');
const router = express.Router();
const UserController = require('../controllers/UserController.js');
const auth = require('../auth.js');


router.post('/register', (request, response) => {
	UserController.registerUser(request.body).then((result) => {
		response.send(result);
	})
})

router.post('/login', (request, response) => {
	UserController.loginUser(request, response);
})

module.exports = router;