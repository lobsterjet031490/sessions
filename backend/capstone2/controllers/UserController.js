const User = require('../models/User.js');
const bcrypt = require('bcrypt');
const auth = require('../auth.js');


module.exports.registerUser = (request_body) => {
	let new_user = new User({
		firstName: request_body.firstName,
		lastName: request_body.lastName,
		email: request_body.email,
		password: bcrypt.hashSync(request_body.password, 10)
	});

	return new_user.save().then((register_user, error) => {
		if(error){
			return {
				message: error.message
			}
		}

		return {
			message: 'Succesfully registered a user!'
		};
	}).catch(error => console.log(error));
}

module.exports.loginUser = (request, response) =>{
	return User.findOne({email: request.body.email}).then((result) =>{
		if(result == null){
			return response.send({
				message: "The user isn't registered yet."
			})
		}
		const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

		if(isPasswordCorrect){
			return response.send({accessToken: auth.createAccessToken(result)});
		} else {
			return response.send({
				message: 'Your email or password is incorrect'
			})
		}
	}).catch(error => response.send(error));
}