const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
require('dotenv').config();
const port = 4000;
const userRoutes = require('./routes/userRoutes.js');

const app = express();

app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());

app.use('/api/users', userRoutes);

mongoose.connect(`mongodb+srv://admin:${process.env.MONGODB_PASSWORD}@batch303-gastardo.erxt430.mongodb.net/gastardo-capstone2?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

mongoose.connection.on('error', () => console.log("Can't connect to database"));
mongoose.connection.once('open', () => console.log('Connected to MongoDB'));

app.listen(process.env.PORT || port, () => {
	console.log(`Capstone-2 is now running at localhost:${process.env.PORT || port}`);
})

module.exports = app;